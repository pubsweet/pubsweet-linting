module.exports = {
  extends: ["stylelint-config-standard"],
  plugins: ["stylelint-order"],
  rules: {
    "declaration-block-no-duplicate-properties": true,
    "order/order": ["custom-properties", "declarations"],
    "order/properties-alphabetical-order": true,
    "selector-pseudo-class-no-unknown": [
      true,
      {
        ignorePseudoClasses: ["global"]
      }
    ]
  }
};
